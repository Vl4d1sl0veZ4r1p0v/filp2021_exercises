package exercises04.calculator

import scala.Integral.Implicits.infixIntegralOps

class Calculator[T: Integral] {
  def isZero(t: T): Boolean =
    t == implicitly[Integral[T]].zero

  def operation(left: Expr[T], right: Expr[T], f: (T, T) => T): Result[T] = {
    calculate(left) match {
      case Success(value_first) =>
        calculate(right) match {
          case Success(value_second) => Success(f(value_first, value_second))
          case DivisionByZero        => DivisionByZero
        }
      case DivisionByZero => DivisionByZero
    }
  }

  def calculate(expr: Expr[T]): Result[T] = expr match {
    case Mul(left, right) => operation(left, right, (_ * _))
    case Div(left, right) => {
      calculate(right) match {
        case Success(value_second) => {
          if (value_second == 0) DivisionByZero
          else {
            calculate(left) match {
              case Success(value_first) =>
                Success(value_first / value_second)
              case DivisionByZero => DivisionByZero
            }
          }
        }
        case DivisionByZero => DivisionByZero
      }
    }
    case Plus(left, right)  => operation(left, right, (_ + _))
    case Minus(left, right) => operation(left, right, (_ - _))
    case Val(v)             => Success(v)
    case If(iff, cond, left, right) => {
      val condition_result = cond match {
        case Mul(left, right)               => calculate(Mul(left, right))
        case Div(left, right)               => calculate(Div(left, right))
        case Plus(left, right)              => calculate(Plus(left, right))
        case Minus(left, right)             => calculate(Minus(left, right))
        case Val(v)                         => calculate(Val(v))
        case If(iff_, cond_, left_, right_) => calculate(If(iff_, cond_, left_, right_))
      }
      condition_result match {
        case Success(value) => {
          if (iff(value)) calculate(left)
          else calculate(right)
        }
        case DivisionByZero => DivisionByZero
      }
    }
  }
}
