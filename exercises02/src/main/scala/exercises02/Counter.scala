package exercises02

object Counter {
  private val SeparatorsPattern = "[.,!?:\n\t\r\\s()]"
  private val SeparatorsPatternNumbers = "[!?:\n\\s()]"
  private val EnglishWordsPattern = "[a-zA-Z'-]+"
  private val NumbersPattern = "[0-9.,]+"

  /**
    * Посчитать количество вхождений слов в тексте
    * слово отделено символами [\s.,!?:\n\t\r]
    */
  def countWords(text: String): Map[String, Int] = {
    text
      .split(SeparatorsPattern)
      .filterNot(_.isEmpty)
      .groupMapReduce(_.toLowerCase)(_ => 1)(_ + _)
  }

  /**
    * Посчитать количество вхождений английских слов в тексте
    * слово отделено символами [\s.,!?:\n\t\r]
    */
  def countEnglishWords(text: String): Map[String, Int] = {
    text
      .split(SeparatorsPattern)
      .filter(s => s.matches(EnglishWordsPattern))
      .groupMapReduce(_.toLowerCase)(_ => 1)(_ + _)
  }

  /**
    * Посчитать количество вхождений чисел в тексте
    * число отделено символами [\s!?:\n\t\r]
    */
  def countNumbers(text: String): Map[String, Int] = {
    text
      .split(SeparatorsPatternNumbers)
      .filter(s => s.matches(NumbersPattern))
      .groupMapReduce(_.toLowerCase)(_ => 1)(_ + _)
  }

}
